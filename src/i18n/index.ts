import zh from "./config/zh";
import en from "./config/en";
import { createI18n } from "vue-i18n";

const messages = {
  zh,
  en,
};
const i18n = createI18n({
  locale: "zh", //当前语言
  fallbackLocale: "en",
  messages,
  legacy: false, //setup 才能使用useI18n
});
export default i18n;
