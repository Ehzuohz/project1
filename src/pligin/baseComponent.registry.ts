import BaseHeader from "@/components/baseHeader/BaseHeader.vue";
import BaseList from "@/components/baseList/index.vue";
import { App } from "vue";
import icon from "@/components/iconFont/index";
export default {
  install(app: App): void {
    app.component("base-header", BaseHeader);
    app.component("base-list", BaseList);
    Object.keys(icon).forEach((key) => {
      app.component(key, icon[key]);
    });
  },
};
