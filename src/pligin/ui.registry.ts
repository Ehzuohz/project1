import { Button, message, Pagination, ConfigProvider, Card } from "ant-design-vue";

const install = (app: any): void => {
  app.use(Button);
  app.use(Pagination);
  console.log(ConfigProvider);
  app.use(ConfigProvider);
  app.use(Card);

  app.config.globalProperties.$message = message;
};
export default {
  install,
};
