const statusCode: any = {
  "400": "你输入的参数有问题",
  "401": "你身份不明确,或登录过期,请尝试重新登录",
  "404": "未找到",
  "500": "服务器错误",
};

export default statusCode;
