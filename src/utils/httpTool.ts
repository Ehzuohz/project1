import axios from "axios";
import { message } from "ant-design-vue";
import CODMAP from "./codeMap";
export interface PageNationParams {
  page?: number;
  pageSize?: number;
}

export interface Params extends PageNationParams {
  [propertyName: string]: string | number | boolean | undefined;
}

const httpTool = axios.create({
  timeout: 10000,
  baseURL: process.env.VUE_APP_BASEURL,
});

// 响应拦截
httpTool.interceptors.response.use(
  (response) => {
    if (response.data?.success) {
      return response.data;
    }
    message.error(
      response.data?.msg || CODMAP[response.data?.statusCode || 500]
    );
  },
  (error) => {
    if (error.code === "ECONNABORTED") {
      message.error("网络请求过慢");
    } else {
      message.error(
        error.response?.statusText || CODMAP[error.data?.statusCode || 400]
      );
    }
    return Promise.reject(error);
  }
);

export default {
  // 拦截后的 axios
  ...httpTool,
  // get
  get(url: string, params?: Params) {
    // console.log(url, params);
    return httpTool.get(url, {
      params,
    });
  },
  // post
  post() {
    console.log("post");
  },
};
