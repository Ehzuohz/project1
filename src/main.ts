
import { createApp } from "vue";
import { createPinia } from "pinia";
import App from "./App.vue";
import router from "./router";
import uiPlugin from "@/pligin/ui.registry";
import baseComponent from "@/pligin/baseComponent.registry";
import i18n from "@/i18n";
import "@/styles/var.less";
import "@/styles/common.less";
import "@/styles/theme.css";

// console.log(i18n);
createApp(App)
  .use(createPinia())
  .use(i18n)
  .use(router)
  .use(uiPlugin)
  .use(baseComponent)
  .mount("#app");
