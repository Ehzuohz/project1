import { defineStore } from "pinia";
import { base } from "@/services";
const useStore = defineStore("baseStore", {
  state: () => ({
    navList: [],
    tage: [],
    readList: [],
    typeList: [],

  }),
  actions: {
    // 请求nav数据
    async getNavData() {
      // base(全局整合)
      const { data } = await base.getNavPage();
      this.$patch({
        // 添加至state
        navList: data[0]
          .filter((item: any) => item.id)
          .sort((a: any, b: any) => b.order - a.order),
      });
    },

    async getReadList_s() {
      const { data } = await base.getReadList();
      this.$patch({
        readList: data
      });
      console.log(data, '推荐阅读数据');
    },

    async getTage_s() {
      const { data } = await base.getTage();
      this.$patch({
        tage: data
      });
      console.log(data, '文章标签');

    },

    async getTypeList_s() {
      const { data } = await base.getTypeList();
      this.$patch({
        typeList: data
      });
      console.log(data, '文章分类');
    },
  },
});
export default useStore;
