import { ComponentOptions } from "vue";
const icons = require.context("./icons", true, /\.vue$/);
interface Components {
  [Name: string]: ComponentOptions;
}
const res = icons.keys().reduce((returnValue: Components, item) => {
  const components = icons(item).default;
  returnValue[components.__name] = components;
  return returnValue;
}, {});
export default res;
