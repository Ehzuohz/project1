// axios  以及请求方式
import http from "@/utils/httpTool";
import { Params } from "@/utils/httpTool";

export const getNavPage = (params: Params = {}) =>
  http.get("/api/page", params);

// 文章标签
export const getTage = () =>
  http.get("/api/tag", {
    articleStatus: "publish"
  });

// 推荐阅读
export const getReadList = () =>
  http.get("api/article/recommend");

// 文章分类
export const getTypeList = () =>
  http.get("api/category", {
    articleStatus: "publish"
  });